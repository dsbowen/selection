"""Main analyses.
"""
from __future__ import annotations

import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from multiple_inference.base import ModelBase

from src.utils import PROCESSED_DATA_DIR, RESULTS_DIR, Simulator

DATAFILES = (
    "diversity_agreed.csv",
    "exercise.csv",
    "judges_black.csv",
    "movers.csv",
    "opportunity_atlas.csv",
    "penn.csv",
    "pisa.csv",
    "state_reps.csv",
    "superforecasters.csv",
)
QUANTILE = 0.1, 0.2, 0.8, 0.9
ALPHA = 0.05, 0.1, 0.2
FILENAME_MAP = {
    "superforecasters": "Good Judgment Project",
    "movers": "Movers",
    "opportunity_atlas": "Opportunity atlas",
    "pisa": "PISA",
    "exercise": "24-Hour Fitness experiment",
    "penn": "Flu study",
    "state_reps": "State representatives",
    "diversity_agreed": "Diversity in academia",
    "judges_black": "Discriminatory sentencing",
}
DATASET_ORDER = list(FILENAME_MAP.values())
CONF_INT_ORDER = [
    "Frequentist marginal",
    "Frequentist simultaneous",
    "Bayesian marginal",
    "Bayesian simultaneous",
]
SIMULTANEOUS_CONF_INT_ORDER = [
    method for method in CONF_INT_ORDER if "simultaneous" in method
]
SELECTION_ORDER = [
    "Frequentist FDR CI",
    "Frequentist FWER CI",
    "Bayesian FDR CI",
    "Bayesian FWER CI",
    "Bayesian FDR direct",
    "Bayesian FWER direct",
]
FWER_SELECTION_ORDER = [method for method in SELECTION_ORDER if "FWER" in method]


def analyze_simulations():
    """Analyze simulations."""
    if not os.path.exists(results_dir := os.path.join(RESULTS_DIR, "simulations")):
        os.mkdir(results_dir)

    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "simulation_results.csv")).rename(
        columns={"coverage": "marginal_coverage"}
    )
    df["simultaneous_coverage"] = df.marginal_coverage == 1
    df["approx_simultaneous_coverage"] = df.marginal_coverage >= 0.98
    df.filename = df.filename.map(FILENAME_MAP)

    # analyze confidence intervals
    gb_variables = ["frequentist", "simultaneous", "alpha", "filename"]
    conf_int_df = df[df.tails == 2].drop_duplicates(gb_variables + ["sim_no"])
    conf_int_df["scaled_avg_length"] = conf_int_df.avg_length / conf_int_df.n_params
    conf_int_df = conf_int_df.groupby(gb_variables).mean().reset_index()
    conf_int_df["incorrect_marginal_coverage"] = (
        conf_int_df.marginal_coverage < 1 - conf_int_df.alpha
    )
    conf_int_df["incorrect_simultaneous_coverage"] = (
        conf_int_df.simultaneous_coverage < 1 - conf_int_df.alpha
    )
    conf_int_df["method"] = conf_int_df.apply(
        lambda row: f"{'Frequentist' if row.frequentist else 'Bayesian'} {'simultaneous' if row.simultaneous else 'marginal'}",
        axis=1,
    )
    conf_int_df.to_csv(
        os.path.join(results_dir, "confidence_intervals.csv"), index=False
    )

    # plot marginal coverage
    alphas = conf_int_df.alpha.unique()
    alpha_min, alpha_max = alphas.min(), alphas.max()
    fig, ax = plt.subplots()
    sns.lineplot(
        data=conf_int_df,
        x="alpha",
        y="marginal_coverage",
        hue="method",
        hue_order=CONF_INT_ORDER,
        ci=None,
        ax=ax,
    )
    ax.plot(
        [alpha_min, alpha_max],
        [1 - alpha_min, 1 - alpha_max],
        color=sns.color_palette()[6],
        linestyle="--",
    )
    ax.set_xticks(alphas)
    ax.set_ylabel("Marginal coverage")
    plt.savefig(
        os.path.join(results_dir, "marginal_coverage.png"), bbox_inches="tight", dpi=300
    )

    # plot simultaneous coverage
    sim_conf_int_df = conf_int_df[conf_int_df.simultaneous]
    fig, axes = plt.subplots(1, 2, figsize=(12, 5))
    sns.lineplot(
        data=sim_conf_int_df,
        x="alpha",
        y="simultaneous_coverage",
        hue="method",
        hue_order=SIMULTANEOUS_CONF_INT_ORDER,
        ci=None,
        ax=axes[0],
    )
    sns.lineplot(
        data=sim_conf_int_df,
        x="alpha",
        y="approx_simultaneous_coverage",
        hue="method",
        hue_order=SIMULTANEOUS_CONF_INT_ORDER,
        ci=None,
        ax=axes[1],
    )
    for ax in axes:
        ax.plot(
            [alpha_min, alpha_max],
            [1 - alpha_min, 1 - alpha_max],
            color=sns.color_palette()[6],
            linestyle="--",
        )
        ax.set_xticks(alphas)
    axes[0].set_ylabel("Simultaneous coverage")
    axes[1].set_ylabel("Approximate simultaneous coverage")
    plt.savefig(
        os.path.join(results_dir, "simultaneous_coverage.png"),
        bbox_inches="tight",
        dpi=300,
    )

    # plot marginal coverage when simultaneous coverage is incorrect
    incorrect_filenames = (
        "24-Hour Fitness",
        "Discriminatory sentencing",
        "Movers",
        "State representatives",
    )
    incorrect_df = df[
        df.filename.isin(incorrect_filenames)
        & (df.simultaneous_coverage == False)
        & (df.frequentist == False)
        & (df.simultaneous == True)
        & (df.tails == 2)
    ]
    fig, ax = plt.subplots()
    sns.lineplot(
        data=incorrect_df,
        x="alpha",
        y="marginal_coverage",
        hue="filename",
        ci=None,
        ax=ax,
    )
    ax.set_ylabel("Marginal coverage")
    ax.set_xticks(alphas)
    plt.savefig(
        os.path.join(results_dir, "incorrect_marginal_coverage.png"),
        bbox_inches="tight",
        dpi=300,
    )

    # plot average length
    fig, ax = plt.subplots()
    sns.lineplot(
        data=conf_int_df,
        x="alpha",
        y="scaled_avg_length",
        hue="method",
        hue_order=CONF_INT_ORDER,
        ci=None,
        ax=ax,
    )
    ax.set_ylabel("Average RCI length")
    ax.set_xticks(alphas)
    plt.legend(bbox_to_anchor=(1, 1), loc="upper left")
    plt.savefig(os.path.join(results_dir, "length.png"), bbox_inches="tight", dpi=300)

    # # analyze selection
    df = df.fillna(-1)
    gb_variables = ["frequentist", "simultaneous", "alpha", "quantile", "filename"]
    selection_df = df[
        (df.tails != 2) & (df.selection != "posterior_tails")
    ].drop_duplicates(gb_variables + ["sim_no"])
    selection_df["fwer"] = selection_df.n_false_discovery > 0
    selection_df = selection_df.groupby(gb_variables).mean().reset_index()
    selection_df["fdr"] = selection_df.n_false_discovery / selection_df.n_selected
    selection_df["method"] = selection_df.apply(
        lambda row: f"{'Frequentist' if row.frequentist == True else 'Bayesian'} {'FWER' if row.simultaneous else 'FDR'} {'direct' if row.tails == -1 else 'CI'}",
        axis=1,
    )
    selection_df["n_selected_scaled"] = selection_df.n_selected / selection_df.n_params
    selection_df.to_csv(os.path.join(results_dir, "selection.csv"), index=False)

    alphas = selection_df.alpha.unique()
    alpha_min, alpha_max = alphas.min(), alphas.max()

    # plot false discovery rate
    grid = sns.relplot(
        data=selection_df,
        x="alpha",
        y="fdr",
        hue="method",
        hue_order=SELECTION_ORDER,
        col="quantile",
        ci=None,
        kind="line",
    )
    for axes in grid.axes:
        for ax in axes:
            ax.plot(
                [alpha_min, alpha_max],
                [alpha_min, alpha_max],
                color=sns.color_palette()[6],
                linestyle="--",
            )

    grid.set(ylabel="False discovery rate", xticks=alphas)
    plt.savefig(os.path.join(results_dir, "fdr.png"), bbox_inches="tight", dpi=300)

    # plot family-wise error rate
    grid = sns.relplot(
        data=selection_df[selection_df.simultaneous],
        x="alpha",
        y="fwer",
        hue="method",
        hue_order=FWER_SELECTION_ORDER,
        col="quantile",
        ci=None,
        kind="line",
    )
    for axes in grid.axes:
        for ax in axes:
            ax.plot(
                [alpha_min, alpha_max],
                [alpha_min, alpha_max],
                color=sns.color_palette()[6],
                linestyle="--",
            )

    grid.set(ylabel="Family-wise error rate", xticks=alphas)
    plt.savefig(os.path.join(results_dir, "fwer.png"), bbox_inches="tight", dpi=300)

    # plot proportion of selected candidates
    grid = sns.relplot(
        data=selection_df,
        x="alpha",
        y="n_selected_scaled",
        hue="method",
        hue_order=SELECTION_ORDER,
        col="quantile",
        ci=None,
        kind="line",
        facet_kws={"sharey": False},
    )
    grid.set(ylabel="Proportion of candidates selected", xticks=alphas)
    plt.savefig(
        os.path.join(results_dir, "n_selected.png"), bbox_inches="tight", dpi=300
    )


def analyze_empirical():
    """Analyze empirical results."""
    if not os.path.exists(results_dir := os.path.join(RESULTS_DIR, "empirical")):
        os.mkdir(results_dir)

    if os.path.exists(
        results_file := os.path.join(results_dir, "results.csv")
    ) and os.path.exists(simulators_file := os.path.join(results_dir, "simulators.p")):
        df = pd.read_csv(results_file)
        with open(simulators_file, "rb") as f:
            simulators = pickle.load(f)
    else:
        simulators, results = {}, []
        for filename in DATAFILES:
            model = ModelBase.from_csv(os.path.join(PROCESSED_DATA_DIR, filename))
            simulator = Simulator(model.mean, model.cov)
            simulators[filename[: -len(".csv")]] = simulator
            df = simulator.simulate(ALPHA, QUANTILE)
            df["filename"] = filename[: -len(".csv")]
            df["n_params"] = len(model.mean)
            results.append(df)
        df = pd.concat(results)
        df.to_csv(results_file, index=False)
        with open(simulators_file, "wb") as f:
            pickle.dump(simulators, f)

    # analyze confidence intervals
    conf_int_df = (
        df[df.tails == 2]
        .drop_duplicates(["frequentist", "simultaneous", "alpha", "filename"])
        .reset_index(drop=True)
    )
    conf_int_df["method"] = conf_int_df.apply(
        lambda row: f"{'Frequentist' if row.frequentist else 'Bayesian'} {'simultaneous' if row.simultaneous else 'marginal'}",
        axis=1,
    )
    conf_int_df.filename = conf_int_df.filename.map(FILENAME_MAP)
    conf_int_df["scaled_average_length"] = conf_int_df.avg_length / conf_int_df.n_params
    alphas = conf_int_df.alpha.unique()
    conf_int_df.to_csv(
        os.path.join(results_dir, "confidence_intervals.csv"), index=False
    )

    # plot average confidence interval length
    fig, ax = plt.subplots()
    sns.lineplot(
        data=conf_int_df,
        x="alpha",
        y="scaled_average_length",
        hue="method",
        hue_order=CONF_INT_ORDER,
        ci=None,
        ax=ax,
    )
    ax.set_xticks(alphas)
    ax.set_ylabel("Average RCI length")
    plt.savefig(os.path.join(results_dir, "length.png"), bbox_inches="tight", dpi=300)

    # plot example confidence intervals
    def errorbar_plot(ax, mean, conf_int, yticks, label):
        ax.errorbar(
            x=mean,
            y=yticks,
            xerr=[mean - conf_int[:, 0], conf_int[:, 1] - mean],
            fmt="o",
            label=label,
        )

    def plot_rank_ci(index, simulator, title):
        ax = axes[index]
        params = simulator.marginal_results.params
        columns = [
            int(np.where(params == 1)[0]),
            int(np.where(params == int(len(params) / 2))[0]),
            int(np.where(params == len(params))[0]),
        ]
        yticks = 5 * np.arange(len(columns), 0, -1)

        ranks = (-simulator.bayes_results.params).argsort().argsort()[columns] + 1
        errorbar_plot(
            ax,
            simulator.marginal_results.params[columns],
            simulator.marginal_results.conf_int(alpha=0.2, columns=columns),
            yticks,
            "Frequentist marginal",
        )
        errorbar_plot(
            ax,
            simulator.simultaneous_results.params[columns],
            simulator.simultaneous_results.conf_int(alpha=0.2, columns=columns),
            yticks - 2,
            "Frequentist simultaneous",
        )
        errorbar_plot(
            ax,
            ranks,
            simulator.bayes_results.rank_conf_int(
                alpha=0.2, columns=columns, simultaneous=False
            ),
            yticks - 1,
            "Bayesian marginal",
        )
        errorbar_plot(
            ax,
            ranks,
            simulator.bayes_results.rank_conf_int(alpha=0.2, columns=columns),
            yticks - 3,
            "Bayesian simultaneous",
        )

        if index == 0:
            fig.legend(bbox_to_anchor=(1, 1), loc="upper left")
            ax.set_yticklabels(
                [
                    "Best-performing candidate",
                    "Median candidate",
                    "Worst-performing candidate",
                ]
            )
        else:
            ax.set_yticklabels(len(columns) * [None])

        ax.set_yticks(yticks - 1.5)
        ax.set_xlabel("Rank")
        ax.set_title(title)
        return ax

    keys = "superforecasters", "exercise", "opportunity_atlas"
    fig, axes = plt.subplots(1, len(keys), figsize=(20, 7))
    for i, key in enumerate(keys):
        plot_rank_ci(i, simulators[key], FILENAME_MAP[key])

    plt.savefig(
        os.path.join(results_dir, "example_ci.png"), bbox_inches="tight", dpi=300
    )

    # # analyze selection
    selection_df = df.fillna(-1)[
        (df.tails != 2) & (df.selection != "posterior_tails")
    ].reset_index(drop=True)
    selection_df["method"] = selection_df.apply(
        lambda row: f"{'Frequentist' if row.frequentist == True else 'Bayesian'} {'FWER' if row.simultaneous else 'FDR'} {'direct' if row.tails == -1 else 'CI'}",
        axis=1,
    )
    selection_df["n_selected_scaled"] = selection_df.n_selected / selection_df.n_params
    selection_df.filename = selection_df.filename.map(FILENAME_MAP)
    selection_df.to_csv(os.path.join(results_dir, "selection.csv"), index=False)

    # plot proportion of selected candidates
    grid = sns.relplot(
        data=selection_df,
        x="alpha",
        y="n_selected_scaled",
        hue="method",
        hue_order=SELECTION_ORDER,
        col="quantile",
        facet_kws={"sharey": False},
        kind="line",
        ci=False,
    )
    grid.set(ylabel="Proportion of candidates selected", xticks=alphas)
    plt.savefig(
        os.path.join(results_dir, "n_selected.png"), bbox_inches="tight", dpi=300
    )

    # plot example selection
    datasets = (
        "Good Judgment Project",
        "24-Hour Fitness experiment",
        "Opportunity atlas",
    )
    fig, ax = plt.subplots()
    sns.barplot(
        data=selection_df[
            (selection_df.alpha == 0.2)
            & (selection_df["quantile"] == 0.1)
            & selection_df.filename.isin(datasets)
        ],
        x="filename",
        order=datasets,
        y="n_selected_scaled",
        hue="method",
        hue_order=SELECTION_ORDER,
        ax=ax,
    )
    ax.set_xticklabels(ax.get_xticklabels(), rotation=30)
    ax.set_ylabel("Proportion of candidates selected")
    ax.set_xlabel("Dataset")
    plt.savefig(
        os.path.join(results_dir, "example_selection.png"),
        bbox_inches="tight",
        dpi=300,
    )


if __name__ == "__main__":
    sns.set()
    np.random.seed(0)
    analyze_simulations()
    analyze_empirical()
