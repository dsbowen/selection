"""Paths to data files and results.
"""
import os
from itertools import product

import numpy as np
import pandas as pd
from multiple_inference.bayes import Nonparametric, Normal
from multiple_inference.confidence_set import MarginalRanking, SimultaneousRanking

if not os.path.exists(DATA_DIR := "data"):
    os.mkdir(DATA_DIR)
if not os.path.exists(EXTERNAL_DATA_DIR := os.path.join(DATA_DIR, "external")):
    os.mkdir(EXTERNAL_DATA_DIR)
if not os.path.exists(PROCESSED_DATA_DIR := os.path.join(DATA_DIR, "processed")):
    os.mkdir(PROCESSED_DATA_DIR)
if not os.path.exists(RESULTS_DIR := "results"):
    os.mkdir(RESULTS_DIR)

MIN_PARAMS_NONPARAMETRIC = 30


class Simulator:
    def __init__(self, mean, cov, true_rank=None, n_samples=1000):
        self.true_rank = np.zeros(len(mean)) if true_rank is None else true_rank
        self.simultaneous_results = SimultaneousRanking(mean, cov).fit(
            n_samples=n_samples
        )
        self.marginal_results = MarginalRanking(mean, cov).fit(n_samples=n_samples)
        bayes_cls = Nonparametric if len(mean) > MIN_PARAMS_NONPARAMETRIC else Normal
        self.bayes_results = bayes_cls(mean, cov).fit(n_samples=n_samples)
        self.prior = self.bayes_results.model.get_marginal_prior(0)

    def simulate(self, alphas, quantiles):
        return pd.concat([self._simulate_alpha(alpha, quantiles) for alpha in alphas])

    def _simulate_alpha(self, alpha, quantiles):
        conf_ints = []

        conf_int = self.simultaneous_results.conf_int(alpha)
        conf_ints.append(
            {
                "frequentist": True,
                "simultaneous": True,
                "tails": 2,
                "conf_int": conf_int,
            }
        )
        conf_ints.append(
            {
                "frequentist": True,
                "simultaneous": True,
                "tails": 1,
                "conf_int": conf_int,
            }
        )

        conf_ints.append(
            {
                "frequentist": True,
                "simultaneous": False,
                "tails": 2,
                "conf_int": self.marginal_results.conf_int(alpha),
            }
        )
        conf_int = self.marginal_results.conf_int(2 * alpha)
        conf_int[:, 0] = 1
        conf_ints.append(
            {
                "frequentist": True,
                "simultaneous": False,
                "tails": 1,
                "conf_int": conf_int,
            }
        )

        for simultaneous, tails in product((True, False), (1, 2)):
            if tails == 2:
                conf_int = self.bayes_results.rank_conf_int(
                    alpha, simultaneous=simultaneous
                )
            else:
                conf_int = self.bayes_results.rank_conf_int(
                    2 * alpha, simultaneous=simultaneous
                )
                conf_int[:, 0] = 1

            conf_ints.append(
                {
                    "frequentist": False,
                    "simultaneous": simultaneous,
                    "tails": tails,
                    "conf_int": conf_int,
                }
            )

        df = pd.DataFrame(conf_ints)
        df["coverage"] = df.conf_int.apply(
            lambda conf_int: (
                (conf_int[:, 0] <= self.true_rank) & (self.true_rank <= conf_int[:, 1])
            ).mean()
        )
        df["avg_length"] = df.conf_int.apply(
            lambda conf_int: np.diff(conf_int, axis=1).mean()
        )

        posterior_tails = self.bayes_results.conf_int(2 * alpha)[:, 0]
        df = pd.concat(
            [
                self._select(df.copy(), posterior_tails, alpha, quantile)
                for quantile in quantiles
            ]
        )
        df["alpha"] = alpha
        return df.drop(columns=["conf_int", "selected"])

    def _select(self, df, posterior_tails, alpha, quantile):
        target_rank = round(quantile * len(self.true_rank))
        true_selected = self.true_rank <= target_rank

        # perform selection based on confidence intervals
        df["selection"] = "conf_int"
        df["selected"] = df.conf_int.apply(
            lambda conf_int: conf_int[:, 1] <= target_rank
        )

        selection = [
            {
                "selection": "posterior_tails",
                "selected": posterior_tails > self.prior.ppf(1 - quantile),
            },
            {
                "selection": "direct",
                "simultaneous": True,
                "selected": self.bayes_results.compute_best_params(
                    target_rank, alpha=alpha, criterion="fwer", superset=False
                ),
            },
            {
                "selection": "direct",
                "simultaneous": False,
                "selected": self.bayes_results.compute_best_params(
                    target_rank, alpha=alpha, criterion="fdr", superset=False
                ),
            },
        ]
        df = pd.concat([df, pd.DataFrame(selection)])
        df["n_selected"] = df.selected.apply(lambda selected: selected.sum())
        df["n_false_discovery"] = df.selected.apply(
            lambda selected: (selected & ~true_selected).sum()
        )
        df["quantile"] = quantile
        return df
