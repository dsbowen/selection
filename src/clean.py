"""Clean PISA data.
"""
from __future__ import annotations

import os

import numpy as np
import pandas as pd
import pyreadr
from conditional_inference.base import ModelBase

from src.utils import EXTERNAL_DATA_DIR, PROCESSED_DATA_DIR


def clean_pisa():
    df = pyreadr.read_r(os.path.join(EXTERNAL_DATA_DIR, "pisa.rda"))["pisa"].set_index(
        "jurisdiction"
    )
    model = ModelBase(df.math_score, np.diag(df.math_se**2))
    model.to_csv(os.path.join(PROCESSED_DATA_DIR, "pisa.csv"))


def clean_opportunity_atlas():
    df = pd.read_csv("data/external/cz_outcomes_simple.csv")
    population_df = pd.read_csv("data/external/online_table3-2.csv", header=10)[
        ["CZ ", "Census 2000 population"]
    ]
    df = df.merge(population_df, left_on="cz", right_on="CZ ")
    df = df.sort_values("Census 2000 population").tail(100).set_index("czname")
    model = ModelBase(
        df.kfr_pooled_pooled_p25, np.diag(df.kfr_pooled_pooled_p25_se**2)
    )
    model.to_csv(os.path.join(PROCESSED_DATA_DIR, "opportunity_atlas.csv"))


if __name__ == "__main__":
    clean_pisa()
    clean_opportunity_atlas()
