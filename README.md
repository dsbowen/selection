Ranking and selection analysis
==============================

Analyzing Bayesian ranking and selection procedures.

Code available [here](https://gitlab.com/dsbowen/selection). Data available [here](https://osf.io/2mxdp/). Please note that the OSF repository is for data only and does not contain the code files necessary to analyze them.

## Environment

Click [here](https://gitpod.io/#https://gitlab.com/dsbowen/selection/-/tree/master/) to run a pre-configured cloud environment with Gitpod.

If working locally, you'll need python-3.8 or higher. Set up your environment with:

```
$ pip install -r requirements.txt
```

## Data

To run the analyses starting from the processed data, download the data [here](https://osf.io/2mxdp/). Put the `data` folder in the root directory of this repository (i.e., in the same folder as this README file). See section on external data for data sources.

## Cleaning

To re-run the cleaning code, download the external data files (see instructions below) and use:

```
$ make data
```

The resulting files will be in `data/processed`.

## Simulations

See `simulations/README.md` for instructions on rerunning the simulations.

## Analyses

```
$ make analyze
```

The resulting analyses will be in `results/analysis`.

## Data sources

The Movers data are from the [Inference for Losers](https://swlb1.aeaweb.org/articles?id=10.1257/pandp.20221065) paper.

[Data](https://opportunityinsights.org/data/?geographic_level=101&topic=0&paper_id=1652#resource-listing) for the Opportunity Atlas.

[Data](https://github.com/danielwilhelm/R-CS-ranks/tree/master/data) for PISA.

Superforecasters, 24-Hour Fitness experiment, Flu study, State representatives, Diversity in academia, Discriminatory sentencing data cleaned [here](https://gitlab.com/dsbowen/multiple-inference-paper/-/tree/master).
