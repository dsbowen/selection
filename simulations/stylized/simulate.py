"""Simulate selection procedure.
"""
from __future__ import annotations

import logging
import os
import sys
from itertools import product

import numpy as np
import pandas as pd
from scipy.stats import chi2, multivariate_normal, norm

from src.utils import Simulator

# directory to store results
RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "results")
# params run in each simulation
PARAMS = product(
    (
        10,
        30,
        50,
    ),
    ("norm", "chi2"),
    (1, 2, 3),
)
QUANTILE = .1, .2, .8, .9
ALPHA = 0.05, 0.1, 0.2

logging.basicConfig(level=logging.INFO)

if __name__ == "__main__":
    sim_no = int(sys.argv[1])
    logging.info(f"Running simulation {sim_no}")
    np.random.seed(sim_no)

    results = []
    for n_params, dist, param in PARAMS:
        logging.info(
            f"Sampling {n_params} parameters from a {dist} distribution with param {param}"
        )
        # sample true parameters
        mean = (
            norm.rvs(size=n_params, scale=param)
            if dist == "norm"
            else chi2.rvs(size=n_params, df=param)
        )
        cov = np.identity(n_params)
        df = Simulator(
            multivariate_normal.rvs(mean, cov), cov, (-mean).argsort().argsort() + 1
        ).simulate(ALPHA, QUANTILE).simulate(ALPHA, QUANTILE)
        df["n_params"] = n_params
        df["param"] = param
        results.append(df)

    results = pd.concat(results)
    results["sim_no"] = sim_no

    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    results.to_csv(os.path.join(RESULTS_DIR, f"sim_no={sim_no}.csv"), index=False)
