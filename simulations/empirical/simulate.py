"""Simulate selection procedure.
"""
from __future__ import annotations

import logging
import os
import sys

import numpy as np
import pandas as pd
from conditional_inference.base import ModelBase
from scipy.stats import multivariate_normal

from src.utils import PROCESSED_DATA_DIR, Simulator

# directory to store results
RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "results")
DATAFILES = (
    "diversity_agreed.csv",
    "exercise.csv",
    "judges_black.csv",
    "movers.csv",
    "opportunity_atlas.csv",
    "penn.csv",
    "pisa.csv",
    "state_reps.csv",
    "superforecasters.csv"
)
QUANTILE = .1, .2, .8, .9
ALPHA = 0.05, 0.1, 0.2
MAX_PARAMS = 200

logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":
    sim_no = int(sys.argv[1])
    logging.info(f"Running simulation {sim_no}")
    np.random.seed(sim_no)

    results = []
    for filename in DATAFILES:
        logging.info(f"Analyzing {filename}")
        model = ModelBase.from_csv(os.path.join(PROCESSED_DATA_DIR, filename))
        mean, cov = model.mean, model.cov
        if len(mean) > MAX_PARAMS:
            indices = np.random.choice(np.arange(len(mean)), size=MAX_PARAMS, replace=False)
            mean, cov = mean[indices], cov[indices][:, indices]
        df = Simulator(
            multivariate_normal.rvs(mean, cov), cov, (-mean).argsort().argsort() + 1
        ).simulate(ALPHA, QUANTILE)
        df["n_params"] = len(mean)
        df["filename"] = filename[: -len(".csv")]
        results.append(df)

    results = pd.concat(results)
    results["sim_no"] = sim_no

    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    results.to_csv(os.path.join(RESULTS_DIR, f"sim_no={sim_no}.csv"), index=False)
