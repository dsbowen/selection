# A template repository for running simulations with `qsub`

## Setting up the cluster environment

This is a one-time setup.

1. Load your python version e.g., `module load python/python-3.8.5`. Change `cluster_setup.sh`, `simulate.sh`, and `cluster_teardown.sh` to load the correct python version.
2. In the root directory of the repo, create an environment with `python -m venv venv`
3. Activate the virtual environment `. venv/bin/activate`
4. Upgrade pip `python -m pip install -U pip`
5. Install the requirements `pip install -r requirements.txt`

If you run into an error, check that you're using the pip installed in the virtual environment, not the "gobal" pip:

```
$ which pip
/path/to/repo/venv/bin/pip
```

## Running a simulation

Execute these commands from the repository's root directory, *not* the directory of this README file.

### Set up

1. Login with `qlogin`
2. Run the cluster setup with `sh simulations/cluster_setup.sh <simulation-directory>`, replacing `<simulation-directory>` with:
    1. "simulations/stylized" to reproduce stylized simulations, or
    2. "simulations/empirical" to reproduce simulations based on our empirical data
3. Logout `logout`

### Run

4. Submit the simulations to the cluster with `qsub -t 1-$(wc -l < simulations/variables.txt) simulations/simulate.sh`

### Tear down

5. Login with `qlogin`
6. Tear down the cluster with `sh simulations/cluster_teardown.sh <output-file>`, replacing `<output-file>` with:
    1. "stylized.csv" for stylized simulation results
    2. "empirical.csv" for simulations based on empirical data
7. Logout with `logout`
8. Use a file transfer protocol to transfer `data/processed/<output-file>` to your local machine. DO NOT commit a giant csv of simulation results to the repo.

## Creating a new simulation folder

A minimal simulations folder will contain the following files:

1. `variables.py` should have an attribute `variables` which is an iterable of command line arguments that will be passed to `simulation.py`
2. `simulation.py` should store (uniquely named) results csv files in a `results` folder

## Explanation of tasks

1. `cluster_setup.sh` performs the following tasks:
    1. Reads variables from `<simulation-directory>.variables.variables` and writes them to a file named `variables.txt`
    2. Writes the simulation directory to `root.txt`
    3. Creates a directory to store the cluster output
2. `qsub` performs the following tasks:
    1. Reads the variables from a line of `variables.txt`
    2. Changes into the simulation directory stored in `root.txt`
    3. Runs `simulation.py` with the variables
3. `cluster_teardown.sh` performs the following tasks:
    1. Concatenates the results of the simulations (stored in `<simulation-directory>/results)` and stores them in `<simulation-directory>/data/data.csv`
    2. Removes results files and folders that are no longer needed.
